from tkinter import *
import random

zastava1=['1.png','2.png','3.png','4.png','5.png','6.png','7.png','8.png','9.png','10.png','11.png','12.png','13.png','14.png','15.png','16.png','17.png','18.png',
          '19.png','20.png','21.png','22.png','23.png','24.png']  
seznam=[]

slovar={'1.png':'Francija','2.png':'Ukrajina','3.png': 'Turèija','4.png':'Vatikan','5.png':'Švedska','6.png':'Španija','7.png':'Slovaška','8.png':'Srbija',
        '9.png':'San marino','10.png':'Avstrija','11.png':'Azerbajdžan','12.png':'Estonija','13.png':'Nemčija','14.png':'Islandija','15.png':'Lieschteinstein','16.png':'Litva','17.png':'Luxembourg','18.png':'Monaco',
          '19.png':'Črna gora','20.png':'Norveška','21.png':'Poljska','22.png':'Romunija','23.png':'Slovenija','24.png':'Češka republika'}
pravilne=[]

class Zastave():
    def __init__(self,master):
        Label(master,text='Vpiši ime zastave:').grid(row=2,column=0)
        Label(master,text='Kateri državi pripada zastava?').grid(row=0,column=1)
        
        self.canvas = Canvas(master, width=300, height=300)
        self.canvas.grid(row=0, column=0)

        self.e1=Entry(master)
        self.e1.grid(row=2,column=1)
       
        gumb=Button(master,text='Naslednja',command=self.nove)
        gumb.grid(row=2,column=2)

        preveri=Button(master,text='Oddaj odgovor!',command=self.probaj)
        preveri.grid(row=1,column=2)

        printaj=Button(master,text='Shrani rezultate',command=self.shrani)
        printaj.grid(row=3,column=0)
        
        zastavica=random.choice(zastava1)


        
        zastava=PhotoImage(file=zastavica)
        
       
        
        self.canvas.create_image(70,80,image=zastava)

    def probaj(self):
        self.canvas.delete('all')
        neki=self.e1.get()
        neki1=seznam[len(seznam)-1]
        if neki==slovar.get(neki1):
            return self.canvas.create_text(100,200,text='Pravilno') and pravilne.append('1')
        
        else:
            return self.canvas.create_text(100,200,text='Narobe') and pravilne.append('0')

    
    def shrani(self):
        vsota=0
        with open('rezultati','a+') as f:
            for i in pravilne:
                if i=='1':
                    vsota=vsota+1
                elif i=='0':
                    vsota=vsota-1
         
            f.write('Dosegli ste {0} tock\n'.format(vsota))
     
    def nove(self):

        
        self.canvas.delete('all')
        slika=random.choice(zastava1)
        zastava=PhotoImage(file=slika)
        
        seznam.append(slika)
           
        zastava_label=Label(image=zastava)
        zastava_label.image=zastava
        
        self.canvas.create_image(150,150,image=zastava)
        
    
    

    
Kviz=Tk()
display=Zastave(Kviz)
Kviz.mainloop()
